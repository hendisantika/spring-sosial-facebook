package com.hendisantika.ssofb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SsoFbApplication {

    public static void main(String[] args) {
        SpringApplication.run(SsoFbApplication.class, args);
    }
}
