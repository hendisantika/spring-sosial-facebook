# Spring Boot Sosial Facebook SSO Example

Run this project by this command : `mvn clean spring-boot:run`

### Screenshot

Connect to Facebook :

![Connect to Facebook](img/connect.png "Connect to Facebook")

Connected to Facebook

![Connected to Facebook](img/connected.png "Connected to Facebook")

Facebook Feeds

![Facebook Feeds](img/feeds.png "Facebook Feeds")

